function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function identidade(m)
    res = zeros(m, m)
    for i in 1:m
        for j in 1:m
            if i == j
                res[i, j] = 1
            end
        end
    end
    return res
end

function matrix_pot(M, p)
    res = identidade(size(M)[1])
    for i in 1:p
        res = multiplica(res, M)
    end
    return res
end

function matrix_pot_by_squaring(M, p)
    num = p
    i = 0
    tam = size(M)
    c = identidade(size(M)[1])
    while num != 0
        for k in 1:(num%2)*2^i
            c = multiplica(c, M)
        end
        num = div(num,2)
        i += 1
    end
    return c
end

using LinearAlgebra
function compare_times()
   
    M = Matrix(LinearAlgebra.I, 30, 30)
    @time matrix_pot_by_squaring(M, 10)
    @time matrix_pot(M, 10)
end
compare_times()










